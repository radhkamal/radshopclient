import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    port: 8080,
    proxy: {
      '/api/products': {
        target: "https://radshopserver-v1.onrender.com",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/products, ''),
      }
    }
  },
  build: {
    rollupOptions: {
      external: ['axios'],
    }
  }
})
